(function () {
    let minute = 33;

    if (minute >= 0 && minute <= 15) {
        console.log('first quarter');
    } else if (minute > 15 && minute <= 30) {
        console.log('second quarter');
    } else if (minute > 30 && minute <= 45) {
        console.log('third quarter');
    } else if (minute > 45 && minute <= 60) {
        console.log('fourth quarter');
    } else {
        console.log('out of range');
    }
})();