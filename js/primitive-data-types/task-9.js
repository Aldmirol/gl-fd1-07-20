(function () {
    let month = 8;

    if (month < 3 || month === 12) {
        console.log('winter');
    } else if (month >= 3 && month < 6) {
        console.log('spring');
    } else if (month >= 6 && month < 9) {
        console.log('summer');
    } else {
        console.log('autumn');
    }
})();