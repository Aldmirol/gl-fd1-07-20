(function () {
    //string
    let vocation = 'Fred and Brent spend a week in Spain.';

    let familyCamping = 'It is fun to camp in a tent.';
    
    let darknessWalking = 'I kept bumping into things in the dark.';

    let circus = 'The clown did tricks with a chimpanzee.';

    let healthyProducts = "Milk is good for children's teeth.";

    let prohibited = 'I must not tramp on the flovers.';
    
    let frogLife = 'The frog jumps in the pond and swims off.';

    let crabsNewHome = 'A crab crept into a crack in the rock.';

    let strongWind = 'I can hear twings snapping in the wind.';

    let flowingTap = 'A drip from the tap drops in the sink.';

    //objects
    let car = {
        isDriverDrunk: false,
        color: 'blue',
        wheelsCount: '4',
        isCarMoved: true,
        mirrorsCount: 2
    }

    let dog = {
        pawsCount: 4,
        eyesCount: 2,
        color: 'black',
        isHaveCollar: true,
        collarColor: 'red'
    }

    let hat = {
        color: 'blue',
        isHasPattern: true,
        patternColor: 'white',
        isWarmClothes: true,
        isHandMade: false
    }

    let paint = {
        color: 'yellow',
        isPouredIntoBacket: true,
        isCarPaint: false,
        isFresh: true,
        isPescoSmells: true
    }

    let pirate = {
        isHaveHat: true,
        isRealPirate: false,
        eyesCount: 1,
        clothColor: 'black',
        isHaveParrot: true
    }

    let bucket = {
        isEmpty: true,
        color: 'blue',
        handleColor: 'yellow',
        isNew: false,
        handlesCount: 1
    }

    let crayons = {
        crayonsCount: 5,
        isChildrensCrayons: true,
        isHasCase: true,
        oneOfCrayonColor: 'blue',
        caseColor: 'orange'
    }

    let pumpkin = {
        isVegetable: true,
        color: 'orange',
        isTasty: true,
        isGrowsOnEarth: true,
        tailsCount:1
    }

    let rabbit = {
        legsCount: 4,
        eyesCount: 2,
        isHaveFur: true,
        furColor: 'gray',
        isLivesInForest: true
    }

    let banana = {
        isVegetable: false,
        isTasty: true,
        color: 'yellow',
        isFresh: true,
        isGoodSmells: true
    }

    let camera = {
        isJapanMade: true,
        isHasLens: true,
        isWorking: true,
        lensCount: 1,
        color: 'black'
    }

    let dinosaur = {
        isStillAlive: false,
        isBigGrowth: true,
        skinColor: 'brown',
        isAngry: true,
        legsCount: 2
    }

    let elephant = {
        isMammals: true,
        trunksCount: 1,
        skinColor: 'gray',
        isHaveTail: true,
        tusksCount: 2
    }

    let umbrella = {
        color: 'yellow',
        spokesСount: 14,
        handlesCount: 1,
        isOpen: true,
        isDry: false
    }

    let alligator = {
        isPredator: true,
        skinColor: 'green',
        isHaveJaw: true,
        eyesCount: 2,
        tailsCount: 1
    }

    let helicopter = {
        helixCount: 3,
        isFlying: true,
        color: 'red',
        isHasTurbine: true,
        isHasGlasses: true
    }

    let television = {
        isChinaMade: true,
        isWorking: true,
        color: 'black',
        isHasRemoteController: true,
        isFullHd: false
    }

    let watermelon = {
        isTasty: true,
        isHasPeel: true,
        peelColor: 'green',
        isBerry: true,
        tailsCount: 1
    }

    // number
    let iceCreamsCount = 1;

    let shipsCount = 2;

    let underpantsCount = 3;

    let flowersCount = 4;

    let butterflysCount = 5;

    let carsCount = 6;

    let fishCount = 7;

    let dinosaursCount = 8;

    let rabbitsCount = 9;

    let applesCount = 10;

    // boolean
    let isFeelSorryForSomeoneFriendly = true;

    let isPickUpToysFriendly = false;

    let isHaveLunchTogetherUnfriedly = false;

    let isTreatEachOtherFriendly = true;

    let isPushEachOtherFriendly = false;

    let isYellAtEachOtherFriendly = false;

    let isJumpRopeTogetherFriendly = true;

    let isThrowBallToEachOtherFriendly = true;

    let isPinchEachOtherFriendly = false;

    let isReadBooksTogetherFriendly = true;

    let isBiteEachOtherFriendly = false;

    let isPlayBallTogetherFriendly = true;

    let isLectureEachOtherFriendly = false;

    let isOpenDoorForGirlsFriendly = true;

    let isPlayHardFriendly = false;

    let isGiveFiveFriendly = true;

    let isStepUpFriendly = false;

    let isLiftEachOtherFriendly = true;

    let isFightInCarteenFriendly = false;

    let isRunTogetherFriendly = true;

    let isWalkTogetherFriendly = true;
})();