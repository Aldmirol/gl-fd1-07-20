(function () {
    let result = function(a, b) {
        if (a === b) {
            return 1;
        }

        return -1;
    };

    console.log(result('abC', 'abc'));
})();
