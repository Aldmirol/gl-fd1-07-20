(function () {
    function copyright(a = '\u00A9') {
        function result(b) {
            return a + b;
        }

        return result;
    }

    console.log(copyright()('EPAM'));
})();