(function () {
    function getRandomNumber(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }

    let num = getRandomNumber(78000, 80000);

    console.log(String.fromCodePoint(num) + ' ' + num);
})();